import yaml
import os

env = os.environ['env']
bot_name = os.environ['bot_name']
dict_file = {'action_endpoint':{'url':"http://container-"+env+"-rasa-"+bot_name+"-action:5055/webhook"},
             'nlg': {'url': "http://container-"+env+"-rasa-"+bot_name+"-nlg:5066/nlg"}}
with open('endpoints.yml', 'w') as file:
    documents = yaml.dump(dict_file, file)